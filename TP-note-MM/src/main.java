import java.util.List;

public class main {



    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println("Hello ");
        Class c1 = Class.forName("testing.classToTest");
        Class c2 = Class.forName("testing.voiture");
        Class c3 = Class.forName("testing.Maison");
        Class c4 = Class.forName("testing.ITestInterface");
        Class c5 = Class.forName("testing.Emonth");
        Class c6 = Class.forName("testing.ClassNameIsLongerThanThirtyLimit");
        Class c7 = Class.forName("testing.EGoodEnum");
        Class c10 = Class.forName("testing.Player");
        Class c11 = Class.forName("testing.AMonster");
        Class c12 = Class.forName("testing.Castles.DarkCastle");

        List<Class> c = List.of(c1, c2, c3, c4, c5, c6, c7, c10, c11, c12);

        // Mise en place des Linter
        linter l = new linter();
        StructuralLinter sl = new StructuralLinter();

        // Test du Linter
        // Commenter/decommenter les lignes suivantes pour tester le linter
        /*for (Class classes : c) {
            l.checkClassNames(classes); // teste si le nom des classes commence par une majuscule et si le nom est plus petit que 30
        }*/
        //l.checkEnumNames(c7); // teste si le nom des enums commence par une majuscule
        //l.checkMethodNames(c10); // teste si le nom des méthodes commence par une minuscule
        //l.checkAttributeNames(c10); // teste si le nom des attributs commence par une minuscule et en majuscule si constante
        //sl.CheckAbstratClassMethods(c11); // teste si une classe abstraite a au moins une méthode abstraite
        //sl.CheckEnumeratedValues(c7); // teste si un enum a au moins 2 valeurs
        //sl.checkProtectedOrPrivatesClasses(c12); // teste si une classe protégée ou privée a des méthodes ou attributs non publics
    }



}
