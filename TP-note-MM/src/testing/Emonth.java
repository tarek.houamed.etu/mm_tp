package testing;

public enum Emonth {
    JANUARY(1, "January");

    private int Number;
    private String Name;

    Emonth(int Number, String Name) {
        this.Number = Number;
        this.Name = Name;
    }

    public int getNumber() {
        return Number;
    }

}
