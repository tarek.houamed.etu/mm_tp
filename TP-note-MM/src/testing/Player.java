package testing;

public class Player {
    private String name;
    private int score;
    private int level;

    private final int MAX_LEVEL = 10;

    private final int minScore = 0;

    public Player(String name, int score, int level) {
        this.name = name;
        this.score = score;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public int getLevel() {
        return level;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void LevelUp(){
        if (this.level < MAX_LEVEL){
            this.level++;
        }
    }


}
