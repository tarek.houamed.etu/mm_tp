# TP3: Metaprogrammation

Houamed Tarek

# Compilation

Pour compiler le projet, il faut se mettre à la racine du projet et lancer la commande :
    
    ```bash
     javac -d out/ src/*.java src/testing/*.java src/testing/Castles/*.java 
    ```

# Exécution

Pour exécuter le projet, il faut se mettre à la racine du projet et lancer la commande :
    
    ```bash
     java -cp out/ main
    ```

# Tests

Nous avons 12 classes qui sont utilisées pour tester des fonctionnalitées differentes du linter. Ces classes sont dans le package `testing`.
Il suffit de commenter ou decommenter les fonctionnalités dans le fichier `main.java` pour tester les différentes fonctionnalités.

# Fonctionnalités

## 1. Linter : Les noms des classes, interfaces et énumérations doivent commencer par une majuscule et avoir moins de 30 caractères.
    Toutes les classes sauf voiture et testing.ClassNameIsLongerThanThirtyLimit ont un nom qui commence par une majuscule et qui a moins de 30 caractères.

## 2. Linter : Les noms des interfaces doivent commencer par un I et le mot après doit commencer par une majuscule.
    ITestInterface reussi le test mais WrongInterface non.

## 3. Linter : Les noms des énumérations doivent commencer par un E et le mot après doit commencer par une majuscule.
    EGoodEnum reussi le test mais Emonth non.

## 4. Linter : Les noms des classes abstraites doivent commencer par un A et le mot après doit commencer par une majuscule
    AMonster reussi le test.

## 5. Linter : Les noms des méthodes doivent commencer par une minuscule.
    La methode LevelUp de la classe Player ne doit pas reussir le test.

## 6. Linter : Les noms des attributs doivent commencer par une minuscule sauf si ce sont des constantes et dans ce cas, ils doivent être en majuscule.
    Le attributs constant minScore de la classe Player ne doit pas reussir le test (car final).

## 7. Structural Linter : Les classes abstraites doivent avoir au moins une méthode abstraite.
    La classe AMonster  doit reussir le test (methode attack abstraite).

## 8. Structural Linter : Les énumérations doivent avoir au moins deux valeurs énumérées.
    L'énumération EMonth ne doit pas reussir le test mais EGoodEnum oui.

## 9. Structural Linter : Les méthodes ne doivent pas lever directement Exception mais une de ses sous-classes.
    Pas implementé.

## 10. Structural Linter : Les classes protected ou private ne peuvent pas avoir de méthodes ou d’attributs public
    Implementé mais pas testé.

## 11. Structural Linter : Les interfaces doivent être implémentées. On remarquera qu’il n’est pas possible d’avoir directement cette information, mais qu’il est facile pour une classe de savoir les interfaces qu’elle implémente.
    Pas implementé.

## 12. Structural Linter : Les packages doivent avoir au moins deux classes. On remarquera là aussi qu’il n’est pas possible d’avoir directement cette information, mais qu’il est facile pour une classe de connaître son package.
    Implementé mais pas testé.