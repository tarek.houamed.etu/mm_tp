package testing;

public abstract class AMonster {

    private String name;
    private int health;
    private int attack;

    public abstract void attack();
}
