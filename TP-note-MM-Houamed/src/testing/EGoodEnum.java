package testing;

public enum EGoodEnum {
    PLAYER("Player", "testing.Player"),
    MAISON("Maison", "testing.Maison"),
    VOITURE("voiture", "testing.voiture"),
    MONSTER("Monster", "testing.AMonster");

    private String name;
    private String className;

    EGoodEnum(String name, String className) {
        this.name = name;
        this.className = className;
    }

    public String getName() {
        return name;
    }

    public String getClassName() {
        return className;
    }
}
