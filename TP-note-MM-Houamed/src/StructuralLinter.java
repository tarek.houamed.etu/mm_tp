import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class is used to check the structural rules of the java code
 */
public class StructuralLinter extends linter{


    public void CheckAbstratClassMethods(Class c){
        // Check if the class is abstract and has at least one abstract method
        int abstractMethods = 0;
        for(Method m : c.getDeclaredMethods()){ // get all the methods of the class
            if(Modifier.isAbstract(c.getModifiers()) && Modifier.isAbstract(m.getModifiers())){ // check if the class is abstract and the method is abstract
                abstractMethods++;
            }

        }
        if(Modifier.isAbstract(c.getModifiers()) && abstractMethods == 0){ // check if the class is abstract and has no abstract method
            System.out.println("Abstract class " + c.getSimpleName() + " has no abstract method");
        }
        else if(Modifier.isAbstract(c.getModifiers()) && abstractMethods > 0){ // check if the class is abstract and has abstract methods
            System.out.println("Abstract class " + c.getSimpleName() + " has " + abstractMethods + " abstract method(s)");
        }
    }

    public void CheckEnumeratedValues(Class c){
        int enumeratedValues = 0; // count the number of enumerated values
        for(Object o : c.getEnumConstants()){ // get all the enumerated values of the enum
            enumeratedValues++;
        }
        if(enumeratedValues<2){
            System.out.println("Enum " + c.getSimpleName() + " has less than 2 values");
        }
        else {
            System.out.println("Enum " + c.getSimpleName() + " has " + enumeratedValues + " values");
        }
    }

    public void checkProtectedOrPrivatesClasses(Class c){
        if(Modifier.isProtected(c.getModifiers()) || Modifier.isPrivate(c.getModifiers())){
            for (Method m : c.getDeclaredMethods()) {
                if (!Modifier.isPublic(m.getModifiers())) {
                    System.out.println("Class " + c.getSimpleName() + " has public methods");
                    return;
                }
            }
            for (Field f : c.getDeclaredFields()) {
                if (!Modifier.isPublic(f.getModifiers())) {
                    System.out.println("Class " + c.getSimpleName() + " has public attributes");
                    return;
                }
            }
    }
    }


    public void checkImplementedInterfaces(Class c){
        if(c.isInterface()){
            System.out.println("Interface " + c.getSimpleName() + " is implemented by " + c.getInterfaces().length + " classes");
        }
    }

    public void check2ClassesInPackage(Package p,List<Class> classes){
        int classesInPackage = 0;
        for(Class c : classes){
            if(c.getPackage().equals(p)){
                classesInPackage++;
            }
        }
        if(classesInPackage<2){
            System.out.println("Package " + p.getName() + " has less than 2 classes");
        }
        else {
            System.out.println("Package " + p.getName() + " has " + classesInPackage + " classes");
        }
    }

    pu
}
