import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * This class is used to check the naming rules of the java code
 */
public class linter {


    /**
     * This method is used to check if the class name is in CamelCase and had a length less than 30
     * @param c the class to check
     */

    public void checkClassNames(Class c) {
        if (Character.isUpperCase(c.getSimpleName().charAt(0)) && c.getSimpleName().length() <30) { // check if the first letter is uppercase and the length is less than 30
            System.out.println("Class name " + c.getSimpleName() + " is correct");
        } else {
            System.out.println("Class name " + c.getSimpleName() + " is incorrect");
        }

    }

    /**
     * This method is used to check if the Interface name starts with I and had a second letter in uppercase
     * @param c the interface to check
     */

    public void checkInterfaceNames(Class c){
        if(c.isInterface() && c.getSimpleName().startsWith("I") && c.getSimpleName().charAt(1) == Character.toUpperCase(c.getSimpleName().charAt(1)) ){
            System.out.println("Interface name " + c.getSimpleName() + " is correct");
        } else {
            System.out.println("Interface name " + c.getSimpleName() + " is incorrect");
        }

    }

    /**
     * This method is used to check if the Enum name starts with E and had a second letter in uppercase
     * @param c the enum to check
     */

    public void checkEnumNames(Class c){
        if(c.isEnum() && c.getSimpleName().startsWith("E") && c.getSimpleName().charAt(1) == Character.toUpperCase(c.getSimpleName().charAt(1)) ){
            System.out.println("Enum name " + c.getSimpleName() + " is correct");
        } else {
            System.out.println("Enum name " + c.getSimpleName() + " is incorrect");
        }

    }

    /**
     * This method is used to check if the Abstract class name starts with A and had a second letter in uppercase
     * @param c the class to check
     */
    public void checkAbstractNames(Class c){
        if (Modifier.isAbstract(c.getModifiers()) && c.getSimpleName().startsWith("A") && c.getSimpleName().charAt(1) == Character.toUpperCase(c.getSimpleName().charAt(1)) ){
            System.out.println("Abstract name " + c.getSimpleName() + " is correct");
        } else {
            System.out.println("Abstract name " + c.getSimpleName() + " is incorrect");
        }
    }

    /**
     * This method is used to check if the method name starts with a lowercase letter
     * @param c the class to check
     */
    public void checkMethodNames(Class c){
        for (Method m : c.getDeclaredMethods()) {
            if (Character.isLowerCase(m.getName().charAt(0))) {
                System.out.println("Method name " + m.getName() + " is correct");
            } else {
                System.out.println("Method name " + m.getName() + " is incorrect");
            }
        }
    }

    /**
     * This method is used to check if the attribute name starts with a lowercase letter, if its constant the name is in uppercase
     * @param c the class to check
     */

    public void checkAttributeNames(Class c){
        for (Field f : c.getDeclaredFields()) { // get all the fields of the class
            if (Modifier.isFinal(f.getModifiers())) { // check if the field is final
                if (f.getName().equals(f.getName().toUpperCase())) { // check if the field is in uppercase
                    System.out.println("Attribute name " + f.getName() + " is correct");
                } else {
                    System.out.println("Attribute name " + f.getName() + " is incorrect");
                }
            }

            else if (Character.isLowerCase(f.getName().charAt(0))) { // check if the field starts with a lowercase letter
                System.out.println("Attribute name " + f.getName() + " is correct");
            }
            else {
                System.out.println("Attribute name " + f.getName() + " is incorrect");
            }

        }
    }


}
